/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:48 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:48 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			**ft_swap_tab(char **tab, int start, int end)
{
	char		*tmp;
	char		**ret;

	ret = tab;
	tmp = ret[start];
	ret[start] = ret[end];
	ret[end] = tmp;
	return (ret);
}
