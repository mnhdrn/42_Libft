/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/27 17:33:46 by clrichar          #+#    #+#             */
/*   Updated: 2018/01/27 17:33:46 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strnstr(const char *big, const char *sml, size_t len)
{
	size_t		i;
	size_t		k;
	const char	*s1;
	const char	*s2;

	i = 0;
	k = ft_strlen(sml);
	if (*sml == '\0')
		return ((char *)big);
	while (i < len && *big)
	{
		s1 = big;
		s2 = sml;
		while (*s1 == *s2 && *s2)
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0' && ((i + k) <= len))
			return ((char *)big);
		i++;
		big++;
	}
	return (NULL);
}
