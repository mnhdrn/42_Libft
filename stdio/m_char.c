/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_char.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <clrichar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/01 13:15:46 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:48:43 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void			wide_char(void)
{
	wchar_t			c;
	char			*ret;
	t_dna			*dna;

	dna = call();
	c = (wchar_t)va_arg(dna->va, wint_t);
	ret = unicode(c);
	if (ret)
	{
		FLAG.s_flag = ret;
		if (c == '\0')
			FLAG.len += 1;
		else
			FLAG.len += (long)ft_strlen(ret);
	}
}

static void			simple_char(void)
{
	int				c;
	char			*ret;
	t_dna			*dna;

	dna = call();
	c = va_arg(dna->va, int);
	ret = NULL;
	if (!(ret = ft_strnew(1)))
		return ;
	if (ret && (c >= 0 && c < 256))
	{
		*ret = (char)c;
		FLAG.s_flag = ret;
		FLAG.len += 1;
	}
}

static void			cancel(void)
{
	t_dna			*dna;

	dna = call();
	if ((FLAG.modifier >> 1 & 1) == 0)
		FLAG.v_padding = 0;
	else if ((FLAG.modifier >> 1 & 1) == 1)
	{
		FLAG.v_padding += FLAG.v_size;
		FLAG.v_size = 0;
	}
}

bool				ex_char(char *s)
{
	char		c;

	c = s[ft_strlen(s) - 1];
	if (c == 'c' && ft_strnchr(s, 'l') == 0)
		simple_char();
	else if (c == 'C')
		wide_char();
	else if (c == 'c' && ft_strnchr(s, 'l') == 1)
		wide_char();
	cancel();
	return (true);
}
