/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   m_misc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clrichar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/02 16:26:59 by clrichar          #+#    #+#             */
/*   Updated: 2018/06/29 17:49:23 by clrichar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

bool			ex_percent(char *s)
{
	t_dna		*dna;

	dna = call();
	if (ft_strnchr(s, '%') == 2)
	{
		FLAG.s_flag = ft_strdup("%");
		FLAG.len = (long)ft_strlen(FLAG.s_flag);
	}
	FLAG.v_padding = 0;
	if ((FLAG.modifier >> 1 & 1) == 1)
	{
		FLAG.v_padding = FLAG.v_size;
		FLAG.v_size = 0;
	}
	return (true);
}

bool			ex_other(char *s)
{
	t_dna		*dna;

	dna = call();
	while (*s && (ft_strnchr("%#- +.", *s) || ft_isdigit(*s)))
		s++;
	FLAG.s_flag = ft_strdup(s);
	FLAG.len = (long)ft_strlen(FLAG.s_flag);
	if ((FLAG.modifier >> 1 & 1) == 1)
	{
		FLAG.v_padding = FLAG.v_size;
		FLAG.v_size = 0;
	}
	return (true);
}

static void		process(t_dna *dna)
{
	if ((FLAG.modifier >> 3 & 1) == 1 && FLAG.s_sign == NULL)
		FLAG.s_sign = ft_strjoin_free(set_sign(2), FLAG.s_sign, 3);
	else if ((FLAG.modifier >> 4 & 1) == 1 && FLAG.s_sign == NULL)
		FLAG.s_sign = ft_strjoin_free(set_sign(0), FLAG.s_sign, 3);
	ft_str_tolower(&FLAG.s_flag);
	ft_str_tolower(&FLAG.s_sign);
	if ((FLAG.modifier >> 5 & 1) == 1 && ft_strequ("0", FLAG.s_flag))
	{
		ft_strdel(&FLAG.s_flag);
		FLAG.s_flag = ft_strdup("");
		FLAG.v_padding += (FLAG.v_padding > 0) ? 2 : 0;
	}
}

static void		cancel(t_dna *dna)
{
	if ((FLAG.modifier >> 0 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 0);
	if ((FLAG.modifier >> 3 & 1) == 1 && (FLAG.modifier >> 4 & 1) == 1)
		FLAG.modifier &= (unsigned char)~(1 << 3);
	else if ((FLAG.modifier >> 1 & 1) == 1)
	{
		FLAG.v_padding += FLAG.v_size;
		FLAG.v_size = 0;
	}
}

bool			ex_addr(char *s)
{
	t_dna		*dna;

	dna = call();
	FLAG.value.taddr = va_arg(dna->va, unsigned long);
	FLAG.s_sign = set_sign(4);
	if (FLAG.value.taddr <= 0)
		FLAG.s_flag = ft_strdup("0");
	else
		FLAG.s_flag = ft_utoa_base((unsigned long)FLAG.value.taddr, 16);
	(void)s;
	cancel(dna);
	process(dna);
	FLAG.len = (long)ft_strlen(FLAG.s_flag);
	if (FLAG.v_padding > FLAG.v_size && FLAG.v_size > 0)
		FLAG.v_padding += FLAG.v_size;
	return (true);
}
